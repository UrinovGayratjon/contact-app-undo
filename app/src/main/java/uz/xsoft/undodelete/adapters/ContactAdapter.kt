package uz.xsoft.undodelete.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_contact.view.*
import uz.xsoft.undodelete.R
import uz.xsoft.undodelete.models.ContactData

class ContactAdapter : RecyclerView.Adapter<ContactAdapter.ViewHolder>() {
    private val contactList = ArrayList<ContactData>()

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(position: Int) {
            val tempData = contactList[position]
            itemView.textName.text = tempData.name
            itemView.textNumber.text = tempData.number
        }
    }

    override fun getItemCount(): Int = contactList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position)

    fun getItem(position: Int): ContactData = contactList[position]

    fun removeItem(position: Int) {
        contactList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun restoreItem(item: ContactData, position: Int) {
        contactList.add(position, item)
        notifyItemInserted(position)
    }

    fun createItem(data: ContactData) {
        contactList.add(data)
        notifyItemInserted(contactList.size - 1)
    }

    fun editItem(data: ContactData, position: Int) {
        contactList[position] = data
        notifyItemChanged(position)
    }
}