package uz.xsoft.undodelete.dialog

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.WindowManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.dialog_edit_contact.*
import uz.xsoft.undodelete.R
import uz.xsoft.undodelete.models.ContactData

class EditContactDialog(context: Context, var data: ContactData) :
    BottomSheetDialog(context, R.style.SheetDialog) {

    private var listener: ((ContactData) -> Unit)? = null

    init {
        setContentView(
            LayoutInflater.from(context).inflate(
                R.layout.dialog_edit_contact,
                null
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        inputName.setText(data.name)
        inputEmail.setText(data.email)
        inputNumber.setText(data.number)
        buttonSave.setOnClickListener {
            listener?.invoke(
                ContactData(
                    0,
                    inputName.text.toString(),
                    inputEmail.text.toString(),
                    inputNumber.text.toString()
                )
            )
            dismiss()
        }

        buttonCancel.setOnClickListener {
            dismiss()
        }
    }

    override fun show() {
        super.show()
        this.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

    }

    fun setListener(list: ((ContactData)) -> Unit) {
        listener = list
    }

}