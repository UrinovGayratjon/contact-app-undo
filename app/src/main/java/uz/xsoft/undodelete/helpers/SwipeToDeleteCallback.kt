package uz.xsoft.undodelete.helpers

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.LEFT
import androidx.recyclerview.widget.ItemTouchHelper.RIGHT
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import uz.xsoft.undodelete.R


class SwipeToDeleteCallback(
    private val context: Context,
    private val swipeListener: SwipeListener
) :
    ItemTouchHelper.Callback() {
    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
        return makeMovementFlags(
            0,
            LEFT or RIGHT
        )
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: ViewHolder,
        viewHolder1: ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: ViewHolder, direction: Int) {

        if (direction == ItemTouchHelper.LEFT) {
            swipeListener.onLeftSwipe(viewHolder.adapterPosition)
        }

        if (direction == ItemTouchHelper.RIGHT) {
            swipeListener.onRightSwipe(viewHolder.adapterPosition)
        }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

        val itemView: View = viewHolder.itemView
        val itemHeight: Int = itemView.height
        val isCancelled = dX == 0f && !isCurrentlyActive

        if (isCancelled) {
            clearCanvas(
                c,
                itemView.right + dX,
                itemView.top.toFloat(),
                itemView.right.toFloat(),
                itemView.bottom.toFloat()
            )
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            return
        }
        if (dX > 0) {
            Log.d("TTTT", "Chapga x=$dX, y=$dY")
            //Chapdan-o'nga harakat uchun
            val mBackground = ColorDrawable()
            val editDrawable = ContextCompat.getDrawable(context, R.drawable.ic_edit)

            val intrinsicWidth = editDrawable!!.intrinsicWidth
            val intrinsicHeight = editDrawable.intrinsicHeight

            mBackground.color = Color.parseColor("#FFE900")

            mBackground.setBounds(
                itemView.left,
                itemView.top,
                itemView.left + dX.toInt(),
                itemView.bottom
            )
            mBackground.draw(c)

            val editIconTop: Int = itemView.top + (itemHeight - intrinsicHeight) / 2
            val editIconMargin: Int = (itemHeight - intrinsicHeight) / 2
            val editIconLeft: Int = itemView.left + editIconMargin
            val editIconRight: Int = itemView.left + editIconMargin+ intrinsicWidth
            val editIconBottom: Int = editIconTop + intrinsicHeight

            editDrawable.setBounds(
                editIconLeft,
                editIconTop,
                editIconRight,
                editIconBottom
            )
            editDrawable.draw(c)
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        } else {
            Log.d("TTTT", "Chapga x=$dX, y=$dY")
            val mBackground = ColorDrawable()
            val deleteDrawable = ContextCompat.getDrawable(context, R.drawable.ic_delete)
            val intrinsicWidth = deleteDrawable!!.intrinsicWidth
            val intrinsicHeight = deleteDrawable.intrinsicHeight
            mBackground.color = Color.parseColor("#b80f0a")
            mBackground.setBounds(
                itemView.right + dX.toInt(),
                itemView.top,
                itemView.right,
                itemView.bottom
            )
            mBackground.draw(c)

            val deleteIconTop: Int = itemView.top + (itemHeight - intrinsicHeight) / 2
            val deleteIconMargin: Int = (itemHeight - intrinsicHeight) / 2
            val deleteIconLeft: Int = itemView.right - deleteIconMargin - intrinsicWidth
            val deleteIconRight: Int = itemView.right - deleteIconMargin
            val deleteIconBottom: Int = deleteIconTop + intrinsicHeight

            deleteDrawable.setBounds(
                deleteIconLeft,
                deleteIconTop,
                deleteIconRight,
                deleteIconBottom
            )
            deleteDrawable.draw(c)
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }
    }

    private fun clearCanvas(
        c: Canvas,
        left: Float,
        top: Float,
        right: Float,
        bottom: Float
    ) {
        c.drawRect(left, top, right, bottom, Paint())
    }

    override fun getSwipeThreshold(viewHolder: ViewHolder): Float {
        return 0.8f
    }
}

interface SwipeListener {
    fun onLeftSwipe(position: Int)
    fun onRightSwipe(position: Int)
}