package uz.xsoft.undodelete.models

data class ContactData(
    var id: Long,
    var name: String,
    var email: String,
    var number: String
)