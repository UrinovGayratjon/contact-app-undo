package uz.xsoft.undodelete

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import uz.xsoft.undodelete.adapters.ContactAdapter
import uz.xsoft.undodelete.dialog.EditContactDialog
import uz.xsoft.undodelete.dialog.SaveContactDialog
import uz.xsoft.undodelete.helpers.SwipeListener
import uz.xsoft.undodelete.helpers.SwipeToDeleteCallback
import uz.xsoft.undodelete.models.ContactData


class MainActivity : AppCompatActivity() {
    private lateinit var contactAdapter: ContactAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contactAdapter = ContactAdapter()

        listContact.layoutManager = LinearLayoutManager(this)
        listContact.adapter = contactAdapter

        val swipeController = SwipeToDeleteCallback(this, swipeListener)
        val itemTouchHelper = ItemTouchHelper(swipeController)
        itemTouchHelper.attachToRecyclerView(listContact)


        buttonAdd.setOnClickListener {
            val dialog = SaveContactDialog(this@MainActivity)
            dialog.setListener {
                createContact(it)
            }
            dialog.show()
        }
    }

    private val swipeListener = object : SwipeListener {
        override fun onLeftSwipe(position: Int) {
            val item = contactAdapter.getItem(position)
            contactAdapter.removeItem(position)
            val snackbar = Snackbar
                .make(container, "Item was removed from the list.", Snackbar.LENGTH_SHORT)
            snackbar.setAction("Undo") {
                contactAdapter.restoreItem(item, position)
                listContact.scrollToPosition(position)
            }
            snackbar.setActionTextColor(Color.YELLOW)
            snackbar.show()
        }

        override fun onRightSwipe(position: Int) {
            val tempData = contactAdapter.getItem(position)
            editContact(tempData, position)
            val dialog = EditContactDialog(this@MainActivity, tempData)
            dialog.setListener {
                editContact(it, position)
            }
            dialog.show()
        }
    }

    private fun createContact(data: ContactData) {
        contactAdapter.createItem(data)
    }

    private fun editContact(data: ContactData, position: Int) {
        contactAdapter.editItem(data, position)
    }
}
